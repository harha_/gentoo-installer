# Gentoo Installer

This is my personal, very simple "installer" script that does a very simple installation of Gentoo.

## How to use

1. Download a minimal Gentoo linux AMD64 ISO image
2. Boot the image into livecd environment
3. Download this installer using wget
4. Execute `./install.sh`
5. Answer to the questions and let the script do its thing

NOTICE: I highly recommend checking the installer script out before executing it, it's dead-simple and that way you know what to expect.

## Finalizing

Just some notes for myself, things to do after base system is installed and running.

1. Install sudo: https://packages.gentoo.org/packages/app-admin/sudo
2. Create user: https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Finalizing
3. Install gentoolkit: https://wiki.gentoo.org/wiki/Gentoolkit
4. Install vbox guest additions: https://wiki.gentoo.org/wiki/VirtualBox
5. Install xorg: https://wiki.gentoo.org/wiki/Xorg/Guide

## Notes

* Upgrade all pkgs in the world set
  * `emerge --ask --update --deep --changed-use @world`
* List enabled USE flags
  * `portageq envvar USE | xargs -n 1`
